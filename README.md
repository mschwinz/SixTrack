# SixTrack

![CERN logo](CERN-logo.png)

SixTrack is a single particle 6D symplectic tracking code optimized for long term tracking in high energy rings.
It is mainly used for the LHC for dynamic aperture studies, tune optimization, collimation studies.

Main CERN SixTrack page:
http://sixtrack.web.cern.ch/SixTrack/
