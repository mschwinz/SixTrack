#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "CollimationGeometry.h"

//This has the G4 collimator geometry construction
//Size, material, length, gap sizes, tilts.
/*
void BuildCollimator(double material, double length, double gap)
{

}
*/
G4VPhysicalVolume* CollimationGeometry::Construct()
{
	if(Assembled == true)
	{

//	runManager->DefineWorldVolume(CollimatorKey_itr->second->Construct());
//	runManager->PhysicsHasBeenModified();

/*
		std::cout << "DELETING GEOMETRY" << std::endl;
		delete jaw1_phys;
		delete jaw2_phys;
		delete world_log;
		delete Jaw1_log;
		delete Jaw2_log;

		delete world_box;
		delete Jaw1;
		delete Jaw2;

		delete world_phys;
*/
		Assembled = false;
	}
	//Change this to true if there are future geometry issues
	bool DoDebug = false;
	bool OverlapCheck = false;
	if(DoDebug)
	{
		OverlapCheck = true;
		std::cout << "GAP :" << ThisCollimatorJawHalfGap << std::endl;
	}

	//Make materials
	G4NistManager* NManager = G4NistManager::Instance();
	G4Material* Vacuum = NManager->FindOrBuildMaterial("G4_Galactic");

	/*
	* Configure the master "world" box
	* "Note that the G4Box constructor takes as arguments the halfs of the total box size."
	*/
	//convert input into G4 units
	const G4double jaw_x = 50*CLHEP::m / 2.0;
	const G4double jaw_y = 50*CLHEP::m / 2.0;

	//The number from SixTrack is the full gap size
	G4double HalfGap = ThisCollimatorJawHalfGap;
	G4double Length = ThisCollimatorJawLength;

	//We are going to place the gun at zero to keep things simple
	//The box spans from -Jaw length to +Jaw length
	G4double world_x = 100*CLHEP::m;
	G4double world_y = 100*CLHEP::m;
	G4double world_z = Length;

	//Make the world box
	world_box = new G4Box("world_box", world_x, world_y, world_z);

	//Make the left/right jaws
	Jaw1 = new G4Box("Jaw1_box", jaw_x, jaw_y, 0.5*Length);
	Jaw2 = new G4Box("Jaw2_box", jaw_x, jaw_y, 0.5*Length);

	//Make the logical volumes, and attach materials
	world_log = new G4LogicalVolume(world_box, Vacuum, "world_log");
	Jaw1_log  = new G4LogicalVolume(Jaw1,ThisCollimatorJawMaterial,"Jaw1_log");
	Jaw2_log  = new G4LogicalVolume(Jaw2,ThisCollimatorJawMaterial,"Jaw2_log");

	world_phys = new G4PVPlacement(0,G4ThreeVector(), world_log, "world", 0, false, 0);
/*
	G4RotationMatrix rotation  = G4RotationMatrix();
	rotation.rotateZ(CollimatorJawRotation);
	G4double JawPosition = jaw_x + HalfGap;
	G4double pi = 4.0*atan2(1.0,1.0);

	//Placement point: rotate around x,y and place a distance of the jaw gap away. Then rotate the jaw face!
	G4ThreeVector position1 = G4ThreeVector(JawPosition*cos(CollimatorJawRotation), JawPosition*sin(CollimatorJawRotation), 0.5 * Length);
	G4ThreeVector position2 = G4ThreeVector(JawPosition*cos(CollimatorJawRotation+pi), JawPosition*sin(CollimatorJawRotation+pi), 0.5 * Length);
	const G4Transform3D transform1 = G4Transform3D(rotation, position1);
	const G4Transform3D transform2 = G4Transform3D(rotation, position2);

	jaw1_phys = new G4PVPlacement(transform1, Jaw1_log, "jaw1", world_log, false, 0, OverlapCheck);
	jaw2_phys = new G4PVPlacement(transform2, Jaw2_log, "jaw2", world_log, false, 0, OverlapCheck);
*/
	jaw1_phys = new G4PVPlacement(0, G4ThreeVector( (jaw_x)+HalfGap, 0, 0.5*Length), Jaw1_log, "jaw1", world_log, false, 0, OverlapCheck);
	jaw2_phys = new G4PVPlacement(0, G4ThreeVector(-(jaw_x)-HalfGap, 0, 0.5*Length), Jaw2_log, "jaw2", world_log, false, 0, OverlapCheck);

	if(DoDebug)
	{
		G4cout << "Adding new collimator with name: " << ThisCollimatorName << " and material " << ThisCollimatorJawMaterial->GetName() << std::endl;
		G4cout << "Total Jaw Length: " << Length/CLHEP::m <<  "m" << G4endl;
		G4cout << "Jaw Rotation: " << ThisCollimatorJawRotation/CLHEP::rad <<  "rad" << G4endl;
		G4cout << "+ve Jaw position: " << ((jaw_x)+HalfGap)/CLHEP::m << "m" << G4endl;
		G4cout << "-ve Jaw position: " << (-(jaw_x)-HalfGap)/CLHEP::m << "m" << G4endl;
		G4cout << "Jaw Gap: " << (HalfGap)/CLHEP::mm << "mm" << G4endl;
//		G4cout << "t1: " << position1 << G4endl; 
//		G4cout << "t1: " << rotation << G4endl; 
//		G4cout << "t2: " << position2 << G4endl; 
	}

	Assembled = true;
	return world_phys;
}
/*
G4double CollimationGeometry::GetLength()
{
	return CollimatorJawLength;
}

G4double CollimationGeometry::GetHalfGap()
{
	return CollimatorJawHalfGap;
}
G4VPhysicalVolume* CollimationGeometry::GetWorldVolume()
{
	return world_phys;
}
*/

void CollimationGeometry::AddCollimator(std::string name, double length, double gap, double rotation, double offset, std::string Material)
{
	CollimatorSettings NewCollimator;
	NewCollimator.CollimatorJawLength = length;
	NewCollimator.CollimatorJawHalfGap = gap * 0.5;
	NewCollimator.CollimatorJawRotation = rotation;
	NewCollimator.CollimatorJawOffset = offset;
	G4Material* JawMaterial = Mmap->GetMaterial(Material);
	NewCollimator.CollimatorJawMaterial = JawMaterial;
	NewCollimator.CollimatorName = name;

	std::pair<std::string,CollimatorSettings> CollimatorKey;
	CollimatorKey = std::make_pair(name,NewCollimator);

	std::pair<std::map< std::string,CollimatorSettings >::const_iterator,bool > check;
	check = CollimatorKeyMap.insert(CollimatorKey);

	if(check.second == false)
	{
		std::cerr << "ERROR: Multiple definitions of collimator: \"" << name << "\"" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void CollimationGeometry::SetCollimator(std::string CollimatorName)
{
//	std::cout << "Looking for collimator \"" << CollimatorName << "\"" << std::endl;
	std::map<std::string,CollimatorSettings>::iterator CollimatorKey_itr = CollimatorKeyMap.find(CollimatorName);
	if(CollimatorKey_itr == CollimatorKeyMap.end())
	{
		std::cerr << "ERROR: Collimator \"" <<  CollimatorName << "\" not found!" << std::endl;
		exit(EXIT_FAILURE);
	}
	ThisCollimatorJawLength = CollimatorKey_itr->second.CollimatorJawLength;
	ThisCollimatorJawHalfGap = CollimatorKey_itr->second.CollimatorJawHalfGap;
	ThisCollimatorJawRotation = CollimatorKey_itr->second.CollimatorJawRotation;
	ThisCollimatorJawOffset = CollimatorKey_itr->second.CollimatorJawOffset;
	ThisCollimatorJawMaterial = CollimatorKey_itr->second.CollimatorJawMaterial;
	ThisCollimatorName = CollimatorKey_itr->second.CollimatorName;
}
